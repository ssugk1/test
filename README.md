В директории mts находятся:
1. Dockerfile для сборки приложения
2. docker-compose.yaml для разверстки.

В директории prometheus находятся:
1. docker-compose.yaml для разверстки Grafana и Prometheus
2. Файл конфигурации prometheus - prometheus.yml

Так же в корне проекта лежит скриншот нагрузки и экспортированый Dashboard с Grafana.
